[TOC]

**Materia: Actualización Tecnológica**

**Profesor: Sergio Pernas**

**Alumno: Juan De La Cruz Quiroz**

# TP Final Docker - Kubernetes

# Poryecto SitioWeb-NginxDocker-Kubernetes

Crear imagen Nginx-Docker personalizada con una pagina web estatica HTML y realizar con ella un despliegue sobre kubernetes 

## Instalación de Kubernetes Microk8s

## Instalamos el gestor de paquetes Snap
`sudo apt update`
`sudo apt install sna`

## Instalación de  Microk8s
`sudo snap install microk8s --classic`

## Configuramos permisos para no usar root
`sudo usermod -a -G microk8s $USER`
`sudo chown -f -R $USER ~/.kube`

## Verificamos estado de la intalacion
`microk8s status --wait-ready`

## Creamos Alias para simplificar el comando
`echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc`

## Aplicamos los cambios
`source ~/.bashrc`

## Descargar los archivos y directorios para el proyecto subidos a GitLab desde la siguiente URL:
Dentro de cada directorio hay un documanto con Datos adicionales para construiur la imagen Docker, exportar imagenes a Dockerhub y como en  generar los achivos de despliegue de Kubernetes.

`https://gitlab.com/Juancruzquiroz/tpfinal-nginxdocker-kubernetes`

## Desligue de los Pods Kubernetes
Una vez descargado el conetenido del repositorio gitlab comenzamos con el despliegue de kubernetes

`kubectl apply -f nginx-deployment.yaml`

## Verificamos el despliegue
`get pods -o wide`

## Exponemos el despliegue para que sea accesible desde nuestro hostlocal
`expose deployment nginx-dp --type=LoadBalancer --port=80`

##Recompilamos datos y verificamos que este todo Ok

`get ep -o wide` `##Verificamos que el balanceado llegue a los Pods`

`get services` `## Obtenemos la IP del Balanceador para acceder al sitio web`

## Para accder al pagina web
Abrimos un navegador web en el host local y colocamos en el a ip recabada en la instruccion anterior osea la ip del balanceador e indicamos el puerto a utilizar /el nombre de la pagina a mostrar Ejemplo:
`http://cambiar-por-la-ip-del-balanceador:80/index.html`
`http://cambiar-por-la-ip-del-balanceador:80/tetris.html`

## Fin del Proyecto
